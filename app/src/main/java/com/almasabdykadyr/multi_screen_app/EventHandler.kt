package com.almasabdykadyr.multi_screen_app

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner

const val APP_TAG = "APP-EVENTS"

class EventHandler: LifecycleEventObserver {
    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        Log.i(APP_TAG, event.toString())
    }
}