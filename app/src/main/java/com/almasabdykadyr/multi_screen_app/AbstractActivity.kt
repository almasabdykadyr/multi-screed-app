package com.almasabdykadyr.multi_screen_app

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

open class AbstractActivity(
    private val activityName: String = "Abstract activity",
    private val nextActivity: Class<out Activity>,
    private val previousActivity: Class<out Activity>? = null
) : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_abstract)

        lifecycle.addObserver(EventHandler())

        val activityTitle: TextView = findViewById(R.id.text_title)
        activityTitle.text = activityName

        assignActivityToButton(nextActivity, findViewById<Button>(R.id.go_next_button))
        assignActivityToButton(previousActivity, findViewById<Button>(R.id.go_back_button))
    }

//    private fun defineNextActivity(
//        nextActivity: Class<out Activity>, previousActivity: Class<out Activity>?
//    ) {
//        val changeActivityToNext: Button = findViewById(R.id.go_next_button)
//        changeActivityToNext.setOnClickListener {
//            startActivity(Intent(this, nextActivity))
//        }
//
//        val changeActivityToPrevious: Button = findViewById(R.id.go_back_button)
//        changeActivityToPrevious.setOnClickListener {
//            if (previousActivity == null) {
//                finishAffinity()
//            } else {
//                startActivity(Intent(this, previousActivity))
//            }
//        }
//    }

    private fun assignActivityToButton(activity: Class<out Activity>?, button: Button) {
        button.setOnClickListener {
            if (activity == null) {
                finishAffinity()
            } else {
                startActivity(Intent(this, activity))
            }
        }
    }


}